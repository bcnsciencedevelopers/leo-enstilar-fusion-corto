function espec() {
  // Inciar estructura
  edetailing.init(estructura);

  // Para los popups externos
  edetailing.index_presentacio_actual = "0";
  edetailing.index_slide_actual = "19";

  // Para el nombre del account
  edetailing.getAccountName();

  let slider = document.querySelector("#id-range");
  let porcentaje = document.querySelector(".porcentaje");
  let cantidad = document.querySelector(".cantidad");
  let image = document.querySelector(".modelo");
  let prod = document.querySelector(".producto");

  slider.addEventListener("input", function (event) {
    event.preventDefault;
    porcentaje.textContent = slider.value + "%";
    cantidad.textContent =
      (slider.value / 2).toString().replace(".", ",") + "g";

    if (slider.value < 1) {
      prod.innerHTML = "";
    } else if (slider.value < 5) {
      prod.innerHTML = '<img src="./css/img/bote_producto.png">';
    } else if (slider.value < 9) {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    } else if (slider.value < 13) {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    } else if (slider.value < 18) {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    } else if (slider.value < 22) {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    } else if (slider.value < 26) {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    } else {
      prod.innerHTML =
        '<img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png"><img src="./css/img/bote_producto.png">';
    }

    if (slider.value < 10) {
      image.src = `./css/img/modelo_0${slider.value}.png`;
    } else if (slider.value < 20) {
      image.src = `./css/img/modelo_${slider.value}.png`;
    } else if (slider.value < 30) {
      image.src = `./css/img/modelo_${slider.value}.png`;
    } else {
      image.src = `./css/img/modelo_${slider.value}.png`;
    }
  });
}
