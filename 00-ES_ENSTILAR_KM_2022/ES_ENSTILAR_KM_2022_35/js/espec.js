function espec() {

    // Inciar estructura
    edetailing.init(estructura);

    // Para los popups externos
    edetailing.index_presentacio_actual = "0";
    edetailing.index_slide_actual = "2";

    // Para el nombre del account
    edetailing.getAccountName();

    $('#global').append('<div class="btn_pop1" onclick="openPop(\'pop1\')"></div>');
    $("#global").append("<div id='pop1' class='popup'><div class='pop_container'><img src='css/img/pop1.png' alt='pop1'/></div></div>");
	$("#global").append("<div id='btn_close_pop1' class='btn-tancar' onclick='closePop(\"pop1\")'></div>");
    $("#btn_close_pop1").appendTo("#pop1");

    $('#global').append('<div class="btn_pop2" onclick="openPop(\'pop2\')"></div>');
    $("#global").append("<div id='pop2' class='popup'><div class='pop_container'><img src='css/img/pop2.png' alt='pop2'/></div></div>");
	$("#global").append("<div id='btn_close_pop2' class='btn-tancar' onclick='closePop(\"pop2\")'></div>");
    $("#btn_close_pop2").appendTo("#pop2");

    $('#global').append('<div class="btn_pop3" onclick="openPop(\'pop3\')"></div>');
    $("#global").append("<div id='pop3' class='popup'><div class='pop_container'><img src='css/img/pop3.png' alt='pop3'/></div></div>");
	$("#global").append("<div id='btn_close_pop3' class='btn-tancar' onclick='closePop(\"pop3\")'></div>");
    $("#btn_close_pop3").appendTo("#pop3");

    $('#global').append('<div class="btn_pop4" onclick="openPop(\'pop4\')"></div>');
    $("#global").append("<div id='pop4' class='popup'><div class='pop_container'><img src='css/img/pop4.png' alt='pop4'/></div></div>");
	$("#global").append("<div id='btn_close_pop4' class='btn-tancar' onclick='closePop(\"pop4\")'></div>");
    $("#btn_close_pop4").appendTo("#pop4");

    $('#global').append('<div class="btn_pop5" onclick="openPop(\'pop5\')"></div>');
    $("#global").append("<div id='pop5' class='popup'><div class='pop_container'><img src='css/img/pop5.png' alt='pop5'/></div></div>");
	$("#global").append("<div id='btn_close_pop5' class='btn-tancar' onclick='closePop(\"pop5\")'></div>");
    $("#btn_close_pop5").appendTo("#pop5");

    $('#global').append('<div class="btn_pop6" onclick="openPop(\'pop6\')"></div>');
    $("#global").append("<div id='pop6' class='popup'><div class='pop_container'><img src='css/img/pop6.png' alt='pop6'/></div></div>");
	$("#global").append("<div id='btn_close_pop6' class='btn-tancar' onclick='closePop(\"pop6\")'></div>");
    $("#btn_close_pop6").appendTo("#pop6");
}
